<?php
	require_once('library/Rain/autoload.php');

	include('inc/Database.inc.php');
	include('inc/Chronos.inc.php');
	include('inc/Sessions.inc.php');
	include('inc/Objectives.inc.php');
	include('inc/LoginManager.inc.php');
	include('inc/TemplateManager.inc.php');
	include('inc/ParameterTools.inc.php');
	include('inc/ErrorManager.inc.php');

	$pagePath = PagePath::getInstance();
	$parameters = Parameters::getInstance();

	include('controllers/userController.php');


	if(!User::hasUsers()) {

		TemplateManager::renderTemplate('setup');

	}

	$user = User::getUserFromCookie();
	if($user == null) {

		TemplateManager::renderTemplate('login');

	}

	TemplateManager::renderTemplate('main');



?>
