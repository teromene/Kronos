<?php
use phpunit\framework\TestCase;

class testSessions extends TestCase {

	public function testSessionsList() {

		$sessionList = Session::getSessions();
		$this->assertCount(4, $sessionList);
		$this->assertContainsOnlyInstancesOf(Session::class, $sessionList);

	}

	public function testSessionElement() {

		$session = new Session(1);
		$this->assertInstanceOf(Session::class, $session);

		$this->assertEquals(1, $session->getId());
		$this->assertEquals(1, $session->getChronoId());
		$this->assertEquals('NuERWQzJhB', $session->getName());
		$this->assertEquals(1326548709, $session->getTimeStart());
		$this->assertEquals(1458121676, $session->getTimeEnd());
		$this->assertEquals(131572967, $session->getLength());
		
		//Test the setters
		$session->setName('test session 1');
		$this->assertEquals('test session 1', $session->getName());
		$session->setTimeEnd(0); //Shouldn't work
		$session->setTimeEnd("lala"); //Shouldn't work
		$session->setTimeEnd("1326548700"); //Shouldn't work
		$this->assertEquals(1458121676, $session->getTimeEnd());
		$session->setTimeEnd(1458121679);
		$this->assertEquals(1458121679, $session->getTimeEnd());

		//Test that all is saved to database
		$session = Session::getSessions()[0];
		$this->assertEquals('test session 1', $session->getName());
		$this->assertEquals(1458121679, $session->getTimeEnd());

	}
}
