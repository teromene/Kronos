<?php
use phpunit\framework\TestCase;

class testChronos extends TestCase {

	public function testChronosList() {

		$chronos = Chrono::getChronos();
		$this->assertCount(6, $chronos);
		$this->assertContainsOnlyInstancesOf(Chrono::class, $chronos);

		$this->assertEquals(6, Chrono::getChronosCount());

	}

	public function testChronoElement() {

		$chronos = Chrono::getChronos();
		$chrono = $chronos[0];

		$this->assertInstanceOf(Chrono::class, $chrono);
		
		//Test ID
		$this->assertEquals(1, $chrono->getId());

		//Test name
		$this->assertEquals("dQj T pHom", $chrono->getName());
		$chrono->setName("test name");
		$this->assertEquals("test name", $chrono->getName());
		$chrono->setName("");
		$this->assertEquals("test name", $chrono->getName());

		//Test color
		$this->assertEquals("#000000", $chrono->getColor());
		$chrono->setColor("#B12D2D");
		$this->assertEquals("#B12D2D", $chrono->getColor());
		$chrono->setColor("Not a color");
		$this->assertEquals("#B12D2D", $chrono->getColor());
		$chrono->setColor("#B12D2DL");
		$this->assertEquals("#B12D2D", $chrono->getColor());

		//Test that all the variables are stored to DB and loading by id, by loading a new object base on the Id
		$newChrono = new Chrono(1);

		$this->assertEquals("test name", $newChrono->getName());
		$this->assertEquals("#B12D2D", $newChrono->getColor());

		//Test isValidChronoId
		$this->assertTrue(Chrono::isValidChronoId(1));
		$this->assertFalse(Chrono::isValidChronoId(-2));
		$this->assertFalse(Chrono::isValidChronoId('test'));
		$this->assertFalse(Chrono::isValidChronoId(7));

		$chronoSessions = $chrono->getSessions();
		$this->assertContainsOnlyInstancesOf(Session::class, $chronoSessions);
		$this->assertCount(3, $chronoSessions);

	}

}
