<?php
namespace Rain\Tpl\Plugin;
require_once __DIR__ . '/../Plugin.php';

class ErrorReporter extends \Rain\Tpl\Plugin{

    protected $hooks = array('beforeParse');

    public function beforeParse(\ArrayAccess $context){

        $html = $context->code;

        if(preg_match_all('/(\{errorbox\})/', $html, $errorStart) && preg_match_all('/(\{\/errorbox\})/', $html, $errorEnd) && preg_match_all('/(\{errormsg\})/', $html, $errorMsg)) {

				$html = str_replace('{errorbox}', '<?php if(ErrorManager::hasError()) { ?>', $html);
				$html = str_replace('{errormsg}', '<?php echo ErrorManager::getErrorMessage(); ?>', $html);
				$html = str_replace('{/errorbox}', '<?php } ?>', $html);

        }

        $context->code = $html;
    }

}
