<?php
namespace Rain\Tpl\Plugin;
require_once __DIR__ . '/../Plugin.php';

class FixClassStatic extends \Rain\Tpl\Plugin{

    protected $hooks = array('beforeParse');

    public function beforeParse(\ArrayAccess $context){

        $html = $context->code;

        if(preg_match_all('/\{static=\"([a-zA-Z0-9]*::[a-zA-Z0-9]*\(.*\))\"\}/', $html, $staticCall)) {

			for($i = 0; $i < count($staticCall[0]); $i++) {

				$html = str_replace($staticCall[0][$i], "<?php echo " . $staticCall[1][$i] ."; ?>", $html);

			}

        }

        $context->code = $html;
    }

}
