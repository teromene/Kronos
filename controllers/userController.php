<?php

if($pagePath->contains('setup') && !User::hasUsers()) {
	if($parameters->isValidString('username') && $parameters->isValidPassword('password') && $parameters->get('password') == $parameters->get('password-confirmation')) {

		User::createUser($parameters->get('username'), $parameters->get('password'));

	} else if($parameters->get('password') != $parameters->get('password-confirmation')) {

		ErrorManager::reportError("The passwords don't match");

	} else {

		ErrorManager::reportError("Please fill all the fields.");

	}
}

if($pagePath->contains('login')) {

	if(User::getUserFromCookie() == null) {
		if($parameters->isValidString('username') && $parameters->isValidPassword('password')) {

			$user = new User($parameters->get('username'));
		
			if($user->checkPassword($parameters->get('password'))) {

				$user->setCookie();
				$pagePath->setPagePath([]);

			}

		}
	}

}
