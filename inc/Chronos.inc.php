<?php
class Chrono {


	const DEFAULT_COLOR = "#000000";
	const COLOR_REGEX = '/\#[0-9a-fA-F]{0,6}$/';

	private $id;
	private $name;
	private $color;
	private $objective_id;
	private $objective;

	public function __construct($id = null, $name = null, $color = null, $objective_id = null) {

		if($id != null && $name != null && $color != null && $objective_id != null) {

			$this->id = $id;
			$this->name = $name;
			$this->color = $color;
			$this->objective_id = $objective_id;

		} else if($id != null && $name == null && $color == null && $objective_id == null) {

			$chronoByIdQuery = Database::getSocket()->query('SELECT * FROM chronos WHERE id = :id');
			$chronoByIdQuery->execute(array(':id' => $id));
			$chronoObjectResult = $chronoByIdQuery->fetchAll(PDO::FETCH_CLASS, 'Chrono');

			if(count($chronoObjectResult) == 1) {

				$tempObject = $chronoObjectResult[0];
				$this->id = $tempObject->getId();
				$this->name = $tempObject->getName();
				$this->color = $tempObject->getColor();
				$this->objective_id = $tempObject->getObjectiveId();

			}

		}

	}

	public function getColor() {

		if(preg_match(self::COLOR_REGEX, $this->color) == 0) {

			$this->setColor(self::DEFAULT_COLOR);

		}
		return $this->color;

	}

	public function setColor($color) {

		if(preg_match(self::COLOR_REGEX, $color) == 1) {

			$colorSetQuery = Database::getSocket()->prepare('UPDATE chronos SET color = :color WHERE id = :id');
			$colorSetQuery->execute(array('color' => $color, 'id' => $this->getId()));
			$this->color = $color;

		}

	}

	public function getId() {

		return $this->id;

	}

	public function getName() {

		return $this->name;

	}

	public function setName($name) {

		if(!empty($name)) {
			$nameSetQuery = Database::getSocket()->prepare('UPDATE chronos SET name = :name WHERE id = :id');
			$nameSetQuery->execute(array('name' => $name, 'id' => $this->getId()));
			$this->name = $name;
		}


	}

	public function getObjectiveId() {

		return $this->objective_id;

	}

	public function setObjectiveId($objective_id) {

		//FIXME: Stub
		if(Objective::isValidObjectiveId($obective_id)) {

			$objectiveIdSetQuery = Database::getSocket()->prepare('UPDATE chronos SET objective_id = :objective_id WHERE id = :id');
			$objectiveIdSetQuery->execute(array('objective_id' => $objective_id, 'id' => $this->getId()));
			$this->objective_id = $objective_id;
			$this->objective = NULL;

		}

	}

	public function getObjective() {

		if($this->objective_id == null) {

			return NULL;

		}

		if($this->objective == null) {

			$this->objective = new Objective($this->objective_id);

		}

		return $this->objective;

	}

	public function getSessions() {

		return Session::getChronoSessions($this->getId());

	}

	public static function getChronos() {

		$chronosListQuery = Database::getSocket()->query('SELECT * FROM chronos');
		$chronosListQuery->execute();
		$chronosList = $chronosListQuery->fetchAll(PDO::FETCH_CLASS, 'Chrono');

		return $chronosList;

	}

	public static function getChronosCount() {

		$chronosCountQuery = Database::getSocket()->query('SELECT COUNT(*) as count FROM chronos');
		$chronosCountQuery->execute();
		$chronosCount = $chronosCountQuery->fetchAll();

		return $chronosCount[0]['count'];

	}

	public static function isValidChronoId($chrono_id) {

		$chronosFilteredQuery = Database::getSocket()->query('SELECT * FROM chronos WHERE id = :id');
		$chronosFilteredQuery->execute(array(':id' => $chrono_id));

		$chronosFiltered = $chronosFilteredQuery->fetchAll();
		
		return count($chronosFiltered) == 1;

	}


}
