<?php
class ErrorManager {

	private static $bHasError = false;
	private static $errorMessage = NULL;

	public static function reportError($errorMessage) {

		self::$bHasError = true;
		self::$errorMessage = $errorMessage;

	}

	public static function hasError()  {

		return self::$bHasError;

	}

	public static function getErrorMessage() {

		return self::$errorMessage;

	}

}
