<?php
class User {

	private $id;
	private $username;
	private $password_hash;

	public function __construct($username) {

		$userFindQuery = Database::getSocket()->prepare('SELECT * FROM users WHERE username = :username');
		$userFindQuery->execute(array('username' => $username));

		$userFindList = $userFindQuery->fetchAll();

		if(count($userFindList) == 1) {

			$this->id = $userFindList[0]['id'];
			$this->username = $userFindList[0]['username'];
			$this->password_hash = $userFindList[0]['password_hash'];

		}
		

	}

	public function setPassword($password) {

		//FIXME: stub

	}

	public function getId() {

		return $this->id;

	}

	public function getUsername() {

		return $this->username;

	}

	public function getPasswordHash() {

		return $this->password_hash;

	}

	public function checkPassword($password) {

		return password_verify($password, $this->getPasswordHash());

	}

	public function login($password) {

		//FIXME: stub

	}

	public function logout($general = false) {

		//FIXME: stub

	}

	public function setCookie() {

		$username = $this->getUsername();
		$token = Token::createToken($this);

		setcookie('kronos-username', $username, strtotime('1 week'), '', '', false, true);
		setcookie('kronos-token', $token, strtotime('1 week'), '', '', false, true);

	}

	public static function getUserFromCookie() {

		if(isset($_COOKIE['kronos-username'])) {

			$username = $_COOKIE['kronos-username'];

		} else {

			return NULL;

		}

		if(isset($_COOKIE['kronos-token'])) {

			$token = $_COOKIE['kronos-token'];

		} else {

			return NULL;

		}

		$user = new User($username);

		if($user->getUsername() != NULL && Token::isValidToken($user, $token)) {

			return $user;

		} else {

			return NULL;

		}

	}

	public static function createUser($username, $password) {

		//FIXME: check that username does not exists
		$password = password_hash($password, PASSWORD_DEFAULT);
		$userCreateQuery = Database::getSocket()->prepare('INSERT INTO users (username, password_hash) VALUES (:username, :password)');
		$userCreateQuery->execute(array('username' => $username, 'password' => $password));

	}

	public static function hasUsers() {

		$userListQuery = Database::getSocket()->query('SELECT * FROM users');
		$userListQuery->execute();

		$userList = $userListQuery->fetchAll();
		
		return (count($userList) >= 1);

	}


}

class Token {

	public static function isValidToken($user, $token) {

		$tokenHash = hash('sha512', $token);
		$userId = $user->getId();
		$time_actual = strtotime('now');

		$tokenGetQuery = Database::getSocket()->prepare('SELECT * FROM tokens WHERE token_string = :token_string AND user_id = :user_id AND expiry_timestamp > :time_actual');
		$tokenGetQuery->execute(array('token_string' => $tokenHash, 'user_id' => $userId, 'time_actual' => $time_actual));

		$tokenList = $tokenGetQuery->fetchAll();
		return (count($tokenList) == 1);

	}

	public static function autoCleanTokens() {

		//FIXME: stub

	}

	public static function createToken($user) {

		$userId = $user->getId();
		$token = bin2hex(openssl_random_pseudo_bytes(120));
		$expiry_timestamp = strtotime('1 week');
		$token_hash = hash('sha512', $token);

		$tokenInsertQuery = Database::getSocket()->prepare('INSERT INTO tokens(user_id, token_string, expiry_timestamp) VALUES (:user_id, :token_string, :expiry_timestamp)');
		$tokenInsertQuery->execute(array('user_id' => $userId, 'token_string' => $token_hash, 'expiry_timestamp' => $expiry_timestamp));

		return $token;

	}

	public static function deleteTokens($user) {

		//FIXME: stub

	}

	public function deleteToken($user, $token) {

		//FIXME: stub

	}


}

