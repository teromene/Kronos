<?php
use Rain\Tpl;
class TemplateManager {

	public static function renderTemplate($templateName, $templateOptions = null) {

		Tpl::registerPlugin(new Tpl\Plugin\ErrorReporter());
		Tpl::registerPlugin(new Tpl\Plugin\FixClassStatic());

		$template = new Tpl;
		$config = array(
			"tpl_dir"       => "templates/",
			"cache_dir"     => "cache/"
		);

		Tpl::configure($config);

		$template->draw($templateName);

		die(); //We die after rendering the template

	}


}
