<?php
class Objective {

	private $id;
	private $duration;
	private $period;

	const DAILY = 1;
	const WEEKLY = 2;
	const MONTHLY = 3;

	public function __construct($id = null, $duration = null, $period = null) {

		if($id != null && $duration != null && self::isValidPeriod($period)) {

			$this->id = $id;
			$this->duration = $duration;
			$this->period = $period;


		} else if($id != null && $duration == null && !self::isValidPeriod($period)) {

			//FIXME: stub

		}

	}

	public function getId() {

		return $this->id;

	}

	public function getDuration() {

		return $this->duration;

	}

	public function setDuration($duration) {

		if($duration > 0) {

			$durationSetQuery = Database::getSocket()->prepare('UPDATE objectives SET duration = :duration WHERE id = :id');
			$durationSetQuery->execute(array('duration' => $duration, 'id' => $this->getId()));
			$this->duration = $duration;

		}

	}

	public function getPeriod() {

		return $this->period;

	}

	public function setPeriod($period) {

		if(self::isValidPeriod($period)) {

			$periodSetQuery = Database::getSocket()->prepare('UPDATE objectives SET duration = :duration WHERE id = :id');
			$periodSetQuery->execute(array('period' => $period, 'id' => $this->getId()));
			$this->duration = $duration;

		}

	}

	private static function isValidPeriod($period) {

		if($period == self::DAILY || $period == self::WEEKLY || $period == self::MONTHLY) {

			return true;

		} else {

			return false;

		}

	}

	public static function isValidObjectiveId($objective_id) {

		$objectiveFilteredQuery = Database::getSocket()->query('SELECT * FROM objectives WHERE id = :id');
		$objectiveFilteredQuery->execute(array(':id' => $objective_id));

		$objectiveFiltered = $objectiveFilteredQuery->fetchAll();
		
		return count($objectiveFiltered) == 1;

	}

}
