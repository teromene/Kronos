<?php
class Session {

	private $id;
	private $chrono_id;
	private $time_start;
	private $time_end;
	private $name;

	function __construct($id = null, $chrono_id = null, $time_start  = null, $time_end  = null, $name = null) {

		if($id != null && $chrono_id != null && $time_start != null && $name != null) {

			$this->id = $id;
			$this->chrono_id = $chrono_id;
			$this->time_start = $time_start;
			$this->time_end = $time_end;
			$this->name = $name;

		} else if($id != null && $chrono_id == null && $time_start == null && $time_end == null && $name == null)  {

			$sessionByIdQuery = Database::getSocket()->query('SELECT * FROM sessions WHERE id = :id');
			$sessionByIdQuery->execute(array(':id' => $id));
			$sessionObjectResult = $sessionByIdQuery->fetchAll(PDO::FETCH_CLASS, 'Session');

			if(count($sessionObjectResult) == 1) {

				$tempSession = $sessionObjectResult[0];

				$this->id = $tempSession->getId();
				$this->chrono_id = $tempSession->getChronoId();
				$this->time_start = $tempSession->getTimeStart();
				$this->time_end = $tempSession->getTimeEnd();
				$this->name = $tempSession->getName();

			}

		}

	}

	public function getId() {

		return $this->id;

	}

	public function getChronoId() {

		return $this->chrono_id;

	}

	/*FIXME: should we allow sessions to move parent ?
	public function setChronoId($chrono_id) {


	}
	*/

	public function getTimeStart() {

		return $this->time_start;

	}

	/*FIXME: should we allow setting time ?
	public function setTimeStart($time_start) {


	}
	*/

	public function getTimeEnd() {

		return $this->time_end;

	}

	public function setTimeEnd($time_end) {

		if(is_numeric($time_end) && $time_end > $this->getTimeStart()) {

			$timeEndSetQuery = Database::getSocket()->prepare('UPDATE sessions SET time_end = :time_end WHERE id = :id');
			$timeEndSetQuery->execute(array('time_end' => $time_end, 'id' => $this->getId()));
			$this->time_end = $time_end;

		}

	}

	public function getName() {

		return $this->name;

	}

	public function setName($name) {

		if(!empty($name)) {

			$nameSetQuery = Database::getSocket()->prepare('UPDATE sessions SET name = :name WHERE id = :id');
			$nameSetQuery->execute(array('name' => $name, 'id' => $this->getId()));
			$this->name = $name;

		}

	}

	public function getLength() {

		if($this->getTimeEnd() == null) {

			return null;

		}
		return $this->getTimeEnd() - $this->getTimeStart();

	}

	public static function getChronoSessions($chronoId) {

		$sessionListQuery = Database::getSocket()->query('SELECT * FROM sessions WHERE chrono_id = :chrono_id');
		$sessionListQuery->execute(array('chrono_id' => $chronoId));
		$sessionList = $sessionListQuery->fetchAll(PDO::FETCH_CLASS, 'Session');

		return $sessionList;

	}


	public static function getSessions() {

		$sessionListQuery = Database::getSocket()->query('SELECT * FROM sessions');
		$sessionListQuery->execute();
		$sessionList = $sessionListQuery->fetchAll(PDO::FETCH_CLASS, 'Session');

		return $sessionList;

	}

	public function hasStartedSessions() {

		$startedSessionQuery = Database::getSocket()->query('SELECT id FROM sessions WHERE time_end IS NULL LIMIT 1');
		$startedSessionQuery->execute();
		$startedSession = $startedSessionQuery->fetchAll();

		return (count($startedSession) > 0);

	}

	public static function getStartedSessions() {

		$startedSessionListQuery = Database::getSocket()->query('SELECT * FROM sessions WHERE time_end IS NULL');
		$startedSessionListQuery->execute();
		$startedSessionList = $startedSessionListQuery->fetchAll(PDO::FETCH_CLASS, 'Session');

		return $startedSessionList;

	}

	public static function getSessionsCount() {

		$sessionsCountQuery = Database::getSocket()->query('SELECT COUNT(*) as count FROM sessions');
		$sessionsCountQuery->execute();
		$sessionsCount = $sessionsCountQuery->fetchAll();

		return $sessionsCount[0]['count'];

	}

}
