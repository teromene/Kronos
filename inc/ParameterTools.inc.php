<?php
class Parameters {

	private static $instance;

	public static function getInstance() {

		if(Parameters::$instance == null) Parameters::$instance = new Parameters();

		return Parameters::$instance;

	}

	private function __construct() {


	}

	public function get($key) {

		return $_POST[$key];

	}

	public function isValidString($key) {

		return (isset($_POST[$key]) && !empty($_POST[$key]));

	}

	public function isValidPassword($key) {

		//FIXME: check password length, chars...
		return self::isValidString($key);

	}

}

class PagePath {

	private static $instance;

	public static function getInstance() {

		if(PagePath::$instance == null) PagePath::$instance = new PagePath();

		return PagePath::$instance;

	}

	private function __construct() {


	}

	public function contains($string) {

		return isset($_GET[$string]);

	}

	public function invalidate($string, $newPath) {

		$_GET[$string] = NULL;
		$_GET[$newPath] = "";

	}

	public function setPagePath($path) {

		$_GET = $path;

	}

}
