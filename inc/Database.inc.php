<?php
class Database {

	private static $path = 'datas.sqlite';
	private static $database;
	var $socket;

	private function __construct() {

		try {

			$this->socket = new PDO('sqlite:' . self::$path);
			$this->socket->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			$this->socket->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		} catch(Exception $e) {

			die("Unable to connect to the sqlite3 database :" . $e);

		}

	}

	public static function setDatabasePath($path) {

		//FIXME: check if is valid path
		self::$path = $path;

	}

	public static function getSocket() {

		if(Database::$database == null) Database::$database = new Database();
		
		return Database::$database->socket;

	}


}
