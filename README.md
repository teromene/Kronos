### Kronos
*A simple time-tracking PHP application*
![Build](http://framagit.org/teromene/kronos/badges/master/build.svg)

## Install
To install Kronos, just drop the folder to your PHP installation.
Make sure that the `cache` folder and the database are readable/writable.

## Features
* Time tracking on different categories
* Ability to resume/edit previously reccorded sessions
* Ability to create objectives and to see your current progression

